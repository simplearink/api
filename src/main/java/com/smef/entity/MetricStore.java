package com.smef.entity;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Map;

@XmlRootElement
public class MetricStore {
    private Integer id;
    private Integer regionId;
    private Integer metricCode;
    private Double value;


    public MetricStore(Map<String, String> map) {
        this.setId(map.get("id"));
        this.setRegionId(map.get("regionid"));
        this.setMetricCode(map.get("metriccode"));
        this.setValue(map.get("value"));
    }

    public MetricStore() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setId(String id) {
        this.id = (id == null) ? null : Integer.parseInt(id);
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = (regionId == null) ? null : Integer.parseInt(regionId);
    }

    public Integer getMetricCode() {
        return metricCode;
    }

    public void setMetricCode(Integer metricCode) {
        this.metricCode = metricCode;
    }

    public void setMetricCode(String metricCode) {
        this.metricCode = (metricCode==null)?null:Integer.parseInt(metricCode);
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
    public void setValue(String value) {
        this.value = (value==null)?null:Double.parseDouble(value);
    }

    @Override
    public String toString() {
        return "MetricStore [id=" + id + ", regionId=" + regionId + ", metricCode=" + metricCode + ", value=" + value
                + "]";
    }


}
