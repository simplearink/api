package com.smef.utils;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class XMLValidator {
    public static final String SMEF_XSD_PATH = "webapps/api/WEB-INF/classes/com/smef/data/smef_schema.xsd";
    public static final File SMEF_XSD = new File(SMEF_XSD_PATH);

    public static boolean validateSmefXML(File smef_xml) {
        return validateXML(smef_xml, XMLValidator.SMEF_XSD);
    }

    public static boolean validateXML(File xml, File xsd) {
        try {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(xsd);
            Validator validator = schema.newValidator();
            StreamSource streamSource = new StreamSource(xml);
            validator.validate(streamSource);
        } catch (IOException | SAXException e) {
            System.out.println("Validator exception: " + e.getMessage());
            return false;
        }
        return true;
    }

    public static boolean validateXML(String xmlPath, String xsdPath) {
        return validateXML(new File(xmlPath), new File(xsdPath));
    }

}
