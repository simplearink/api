package com.smef.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.smef.entity.AverageMetricsObject;
import com.smef.entity.CommitDifferenceObject;


public class QueryDB extends Database{


	public QueryDB() {
		super();
	}
	
	
	public List<CommitDifferenceObject> commitDifference(int id1, int id2) {

		String sql = "SELECT r.name, m.short_name, ms.value FROM "
				+ "metric_store ms, region r, project p, metrics m WHERE"
				+ " r.project_id=p.id AND m.id=ms.metric_code"
				+ " AND ms.region_id=r.id AND p.commit_id=" + id1 +
				" ORDER BY r.name, m.short_name";
		System.out.println(sql);
		List<CommitDifferenceObject> values1 = new ArrayList<>();
		try {
			Statement st = getConnection().createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				CommitDifferenceObject t = new CommitDifferenceObject();
				t.setRegionName(rs.getString(1));
				t.setMetricShortName(rs.getString(2));
				t.setValue1(rs.getDouble(3));

				values1.add(t);
			}
		} catch (SQLException e) {
			System.out.println("DB error: " + e);
		}
		
		sql = "SELECT r.name, m.short_name, ms.value FROM "
				+ "metric_store ms, region r, project p, metrics m WHERE"
				+ " r.project_id=p.id AND m.id=ms.metric_code"
				+ " AND ms.region_id=r.id AND p.commit_id=" + id2 + 
				" ORDER BY r.name, m.short_name";
		System.out.println(sql);
		List<CommitDifferenceObject> values2 = new ArrayList<>();
		try {
			Statement st1 = getConnection().createStatement();
			ResultSet rs1 = st1.executeQuery(sql);
			while (rs1.next()) {
				CommitDifferenceObject t1 = new CommitDifferenceObject();
				t1.setRegionName(rs1.getString(1));
				t1.setMetricShortName(rs1.getString(2));
				t1.setValue1(rs1.getDouble(3));

				values2.add(t1);
			}
		} catch (SQLException e) {
			System.out.println("DB error: " + e);
		}
		for (int i = 0; i < values1.size(); i++) 
		{
			
				if (values1.get(i).getRegionName().equals(values2.get(i).getRegionName()) && 
						values1.get(i).getMetricShortName().equals(values2.get(i).getMetricShortName())) 
				{
					values1.get(i).setValue2(values2.get(i).getValue1());				
				}
					
		}
		return values1;
	}
	
	
	

	public List<AverageMetricsObject> averageMetrics(String r, int id1, int id2) {
		
		String sql = "SELECT m.short_name, ms.value FROM metric_store ms, metrics m, region r, project p WHERE"
				+ " r.project_id=p.id AND m.id=ms.metric_code"
				+ " AND ms.region_id=r.id AND r.name=" + r + " AND p.commit_id=" + id1 + 
				" ORDER BY m.short_name";
		System.out.println(sql);
		List<AverageMetricsObject> values1 = new ArrayList<>();
		try {
			Statement st = getConnection().createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				AverageMetricsObject t = new AverageMetricsObject();
				t.setMetricShortName(rs.getString(1));
				t.setAverageValue(rs.getDouble(2));

				values1.add(t);
			}
		} catch (SQLException e) {
			System.out.println("DB error: " + e);
		}
		
		sql = "SELECT m.short_name, ms.value FROM metric_store ms, metrics m, region r, project p WHERE"
				+ " r.project_id=p.id AND m.id=ms.metric_code"
				+ " AND ms.region_id=r.id AND r.name=" + r + " AND p.commit_id=" + id2 + 
				" ORDER BY m.short_name";
		System.out.println(sql);
		List<AverageMetricsObject> values2 = new ArrayList<>();
		try {
			Statement st = getConnection().createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				AverageMetricsObject t = new AverageMetricsObject();
				t.setMetricShortName(rs.getString(1));
				t.setAverageValue(rs.getDouble(2));

				values2.add(t);
			}
		} catch (SQLException e) {
			System.out.println("DB error: " + e);
		}
		for (int i = 0; i < values1.size(); i++) {
				if (values1.get(i).getMetricShortName().equals(values2.get(i).getMetricShortName())) 
				{
					double temp =(double) (values2.get(i).getAverageValue() + values1.get(i).getAverageValue())/2;
					values1.get(i).setAverageValue(temp);			
				}
				
			}
			
		return values1;
		}
		
		
	

}