package com.smef.exceptions;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.*;

@Provider
public class AlreadyExistExceptionMapper implements ExceptionMapper<AlreadyExistException> {
  
	@Override
	public Response toResponse(AlreadyExistException e) {
		return Response.status(Status.CONFLICT)
				       .entity("Item already exsists in database: " + Status.CONFLICT.getReasonPhrase())
					   .build();
	}
}
