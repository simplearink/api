//package com.smef.exceptions;
//import javax.ws.rs.core.Response;
//import javax.ws.rs.core.Response.Status;
//import javax.ws.rs.ext.ExceptionMapper;
//import javax.ws.rs.ext.Provider;
//
//@Provider
//public class GenericExceptionMapper implements ExceptionMapper<RuntimeException> {
//
//    @Override
//    public Response toResponse(RuntimeException exception) {
//        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Something goes wrong: " + Status.INTERNAL_SERVER_ERROR.getReasonPhrase()).build();
//    }
//
//}